from eyja.interfaces.plugins import BasePlugin
from eyja.constants.types import PluginTypes


class PySONTestDBClientPlugin(BasePlugin):
    name = 'pyson-test'
    plugin_type = PluginTypes.STORAGE_CLIENT
